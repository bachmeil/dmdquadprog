module quadprog.base;

import gretl.base, gretl.matrix, gretl.vector;
import std.algorithm.comparison, std.conv, std.stdio;
alias replace = gretl.vector.replace;

struct QPSolution {
	DoubleVector sol;
	double value;
	DoubleVector unconstrained;
	DoubleVector lagrangian;
	int[] iterations;
	int[] active;
	
	void print() {
		sol.print("Solution vector");
		writeln("\nValue of the objective function: ", value);
		unconstrained.print("\nUnconstrained solution");
		lagrangian.print("\nLagrangian vector");
		writeln("\nIterations: ", iterations);
		writeln("Index of active constraints: ", active);
	}
}

QPSolution qpsolve(GretlMatrix dmat, GretlMatrix dvecRaw, GretlMatrix amat, GretlMatrix bvec, int meq=0, bool factorized=false) {
	// Some of the arguments will be changed
	// Should probably dup them first
	int n = dmat.rows;
	int fddmat = n;
	auto sol = DoubleVector(n);
	auto lagr = DoubleVector(amat.cols);
	double crval = 0.0;
	auto dvec = DoubleVector(dvecRaw.rows);
	dvec = dvecRaw;
	int fdamat = n;
	int q = amat.cols;
	auto iact = new int[q];
	iact[] = 0;
	int nact = 0;
	int[] iter = [0, 0];
	int r = min(n, q);
	auto work = DoubleVector(2*n + r*(r+5)/2 + 2*q + 1);
	work = 0.0;
	auto ierr = to!int(factorized);
	
	qpgen2_(dmat.ptr, dvec.ptr, &fddmat, &n, sol.ptr, lagr.ptr, &crval,
		amat.ptr, bvec.ptr, &fdamat, &q, &meq, iact.ptr, &nact, iter.ptr, work.ptr, &ierr);

	QPSolution result;
	result.sol.replace(sol);
	result.value = crval;
	result.unconstrained.replace(dvec);
	result.lagrangian.replace(lagr);
	result.iterations = iter;
	result.active = iact;
	return result;
}

extern(C) {
	void qpgen2_(double *dmat, double *dvec, int *fddmat, int *n,
      double *sol, double *lagr, double *crval,
      double *amat, double *bvec, int *fdamat, int *q,
      int *meq, int *iact, int *nact, int *iter, double *work, int *ierr);
}
