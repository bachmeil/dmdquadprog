# Overview

This package provides functionality to solve quadratic programming problems in D. It is actually an interface to the functionality in R's quadprog package. It is nonetheless fully efficient, because it calls directly into the underlying Fortran procedure, and does not in any way involve the R interpreter. Furthermore, there is no copying of data. Everything is passed into the Fortran procedure as a pointer.

# Installation

You need to have the [embedr](https://bitbucket.org/bachmeil/embedr) and [dmdgretl](https://bitbucket.org/bachmeil/dmdgretl) packages installed in R. Click the links to see how to install those packages. Also, obviously, you will need to install the quadprog package in R. You can then install the dmdquadprog package from inside R:

```
library(devtools)
install_bitbucket("bachmeil/dmdquadprog")
```

# Example

Usage is the same as for the `solve.QP` function in the R package. Here is an example program that replicates the example in the quadprog package manual.

```
import gretl.base, gretl.matrix;
import quadprog.base;
import std.stdio;

void main() {
	auto dmat = eye(3);
	auto dvec = DoubleVector([0.0, 5.0, 0.0]);
	auto amat = DoubleMatrix([[-4.0, 2.0,  0.0],
                              [-3.0, 1.0, -2.0],
                              [ 0.0, 0.0,  1.0]]);
	auto bvec = DoubleVector([-8.0, 2.0, 0.0]);
	QPSolution sol = qpsolve(dmat, dvec, amat, bvec);
	sol.print();
} 
```

To compile and run the example, save this in a file "qpexample.d". Then open R from within that directory and run

```
library(embedr)
dmd("qpexample", c("dmdgretl", "dmdquadprog"))
```

The first argument to `dmd` is the name of the file (without the .d extension) and the second argument specifies that both the dmdgretl and dmdquadprog packages are needed (the embedr package is loaded by default).