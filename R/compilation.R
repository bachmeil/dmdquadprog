modules <- function() {
	c("base")
}

moddir <- function() {
	"quadprog"
}

flags <- function() {
	return(paste0(find.package("quadprog")[1], "/libs/quadprog.so"))
}

deps <- function() {
	return("dmdgretl")
}
